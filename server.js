var express = require("express");
var app = express();
var port = process.env.PORT || 3000;

app.use(express.static("wwwroot"));

app.listen(port, () => {
  console.log("The audience is listening...on port ", port);
})
