var webpack = require("webpack");
var path = require("path");

var config = {
    entry: path.join(__dirname, "src/index.js"),
    output: {
        path: path.join(__dirname, "/wwwroot"),
        filename: "bundle.js"
    },
    module: {
        loaders: [
            {
                test : /\.(js|jsx)?/,
                exclude: path.join(__dirname, "node_modules"),
                loader : "babel-loader",
                query: {
                    presets: ["es2017", "react"],
                    plugins: ["transform-object-rest-spread"]
                }
            },
            {
                test: /\.scss$/,
                exclude: path.join(__dirname, "node_modules"),
                loaders: ["style-loader", "css-loader", "sass-loader"]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/,
                exclude: path.join(__dirname, "node_modules"),
                loader: "file-loader?name=[name].[ext]"
            },
            {
                test: /\.html/,
                exclude: path.join(__dirname, "node_modules"),
                loader: "file-loader?name=[name].[ext]"
            },
            {
                test: /\.json/,
                exclude: path.join(__dirname, "node_modules"),
                loader: "json-loader"
            }
        ]
    }
};

module.exports = config;
