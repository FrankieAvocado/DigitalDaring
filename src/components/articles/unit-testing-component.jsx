import * as React from "react";
require("../../../assets/screenshots/twoWordSayings.png");
require("../../../assets/screenshots/simpleDescribe.png");
require("../../../assets/screenshots/firstTest.png");
require("../../../assets/screenshots/serverSaveExample.png");
require("../../../assets/screenshots/simpleSpy.png");
require("../../../assets/screenshots/simplePromise.png");
require("../../../assets/screenshots/complicatedStub.png");


// export const logTerribleMistake = (server, error) => {
//   server.save(error).then(console.log("hooray!"));
// }


// import * as server from "httpRequestStuff.js"
// import { spy, stub, assert } from 'sinon';
//
// describe("logTerribleMistake", () => {
//   it("should save the error to the server and then cheer", () => {
//     // arrange
//     const logSpy = spy(console, "log");
//     server.save = stub();
//     const errorMessage = "Oh no!  Something happened!";
//     const pinkySwear = new Promise((resolve, reject) => {
//       resolve(); //immediately resolve
//     });
//     server.save.returns(pinkySwear);
//
//     // act
//     logTerribleMistake(server, errorMessage);
//
//     // assert
//     assert.calledWith(server.save, errorMessage);
//     assert.calledWith(logSpy, "hooray!");
//   })
// })


// const twoWordSayings = (a, b) => {
//   if (a === "hello" && b === "world") {
//     return "greetings";
//   } else if (a === "high" && b === "five") {
//     return "low five";
//   } else {
//     return "nonsense";
//   }
// }
//
// describe("twoWordSayings", () => {
//
// });
//
// describe("twoWordSayings", () => {
//
//   it("should recognize hello world as a greeting", () => {
//     // arrange
//     var first = "hello";
//     var second = "world";
//
//     // act
//     var result = twoWordSayings(first, second);
//
//     // assert
//     expect(result).toEqual("greetings");
//   });
//
//
//   it("should recognize high five as needing a low five", () => {
//
//   });
//
//   it("should assume other combinations of words are nonsense", () => {
//
//   });
// });

export class UnitTesting extends React.Component {
  render(){
    return (<section>
      <article>
        <h1>Why Write Tests?</h1>
        <p></p>
      </article>
      <article>
        <h1>What is a "Unit"</h1>
        <p></p>
      </article>
      <article>
        <h1>How to Describe Tests</h1>
        <p>
          I was once told that if all the documentation for a project was lost, it should
          be able to be recreated just from looking at the unit tests.  The more I think about that,
          the more I think it's a great way to describe how you should approach writing tests.
        </p>
        <p>
          In these examples we'll be working with Jasmine and Angular 4, but other test frameworks such as Mocha
          or XUnit have similar ways of describing your tests for other languages and frameworks.  In general, you'll be covering either a
          specific class or object and there will be a group of tests that represent the functionality
          that your target is expected to have.  Here is a JavaScript function that you might want to
          unit test:
        </p>
        <img src="twoWordSayings.png"/>
        <p>
          And here is a simple start to describing this function.  We will begin by just listing out
          the behaviors that we expect to see:
        </p>
        <img src="simpleDescribe.png"/>
        <p>
          You can already see that these definitions sound a lot like user stories.
          Let's flesh one out so that you can get a better idea of what it looks like to write
          an actual test.  We can begin by using the "arrange, act, assert" pattern where
          we break the test up into three major parts.  Arrange is where we setup our test,
          Act is where we actually run the code to be tested, and Assert is where we compare
          expected results against actual results.  Here's what one of our simple tests might
          look like.
        </p>
        <img src="firstTest.png"/>
        <p>
          Now all of this is enough to do very basic tests, but let's face it: the real
          world is much more complicated.  The code you'll be testing will likely be more
          involved.  In order to handle advanced testing, you'll need to know how to lie,
          cheat, and steal your way across the codebase.  From here on out, consider yourself
          a rebel.  If you have a leather jacket and sunglasses then it's time to put them on.
        </p>
      </article>
      <article>
        <h1>What You Will Have to Do</h1>
        <p>
          Unit testing involves tricking a single "unit" into thinking that it's still
          part of the system.  You'll be making fake objects and functions, passing around
          fake data, and generally doing some sneaky stuff.  One of the best libraries
          around for doing this is called "sinon" and that's what we'll be using in these examples.
          For other languages you might use things like MOQ, Mockito or a wide variety of frameworks.
          The important thing is to understand the concepts here.  Once you get the basics down,
          you can apply those to other languages with a small learning curve.
        </p>
        <p>
          Let's start with Sinon's "spy".  This guy let's you replace a single function on
          an object and...uh..."spy"...on what it does.  What if the function we were testing
          actually took in another function and did something with it?  How would we test
          something like this? :
        </p>
        <img src="serverSaveExample.png"/>
        <p>
          The answer is "Spies"!  Spies are awesome, I'll also show you "stubs" later on which
          are similar but a bit more powerful.  Alright, here we go :
        </p>
        <img src="simpleSpy.png"/>
        <p>
          So this is already super sweet.  We can test whether a spied-on function is called
          and also what parameters it was called with.  That's super powerful.  But there's one
          catch here:  that httpRequestStuff server might actually execute back-end calls.  We can
          make sure that our units are isolated during the test by using a stub instead of a spy.
          Here's a more complicated function that we might need to test :
        </p>
        <img src="simplePromise.png"/>
        <p>
          Okay, so now our http service is actually returning a promise and we need
          to figure out how to handle that in our tests.  Good news, we can totally do that.
          One of the powerful parts of Stubs is that we can tell them what to return when
          they are called.  In this case, we instruct the stub to return a shiny new Promise
          that immediately resolves.  This should mean that the "then" get's fired.
          We also spy on our console.log function and verify that it is called with the "hooray!"
          text.
        </p>
        <img src="complicatedStub.png"/>
      </article>
      <article>
        <h1>Actually Writing Tests</h1>
        <p></p>
      </article>
    </section>)
  }
};
