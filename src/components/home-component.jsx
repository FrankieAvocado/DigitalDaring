import * as React from "react";
import * as ReactDOM from "react-dom";
import { UnitTesting }from "./articles/unit-testing-component.jsx";
import { WhatWeBelieve } from "./sections/what-we-believe-component.jsx";
import { OurOfferings } from "./sections/our-offerings-component.jsx";

export const loadApp = () => {
  ReactDOM.render(
      <div className="useful-info">
        <WhatWeBelieve></WhatWeBelieve>
        <article id="opinions">
          <h1>Our Opinions</h1>
          <p>
            We care a lot about how we build things.  As such, we strive to hold ourselves and our team-mates
            to the highest of standards.  Here are a few articles we've written about how to use technologies.
          </p>
        </article>
        <OurOfferings></OurOfferings>
      </div>,
      document.getElementById("Content")
  );
};
