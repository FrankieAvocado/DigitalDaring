import * as React from "react";

export class WhatWeBelieve extends React.Component {
  render(){
    return (
      <article id="beliefs">
        <h1>What We Believe</h1>
        <p>
          We are never satisfied with the status-quo.  We seek out unusual opportunities, strange
          new technology, impossible ideas, and daring adventures.  We want to design and build
          things that make you say stop and stare.
        </p>
      </article>
    )
  }
};
