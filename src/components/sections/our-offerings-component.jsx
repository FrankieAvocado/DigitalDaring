import * as React from "react";
require("../../../assets/custom_pc.jpg");
require("../../../assets/screenshots/snazzyCode.png");

export class OurOfferings extends React.Component {
  render(){
    return (
      <article id="offerings">
        <h1>Our Offerings</h1>

        <section className="image-with-text">
          <img src="snazzyCode.png"/>
          <span>Websites and Mobile Apps</span>
        </section>

        <section className="image-with-text">
          <img src="custom_pc.jpg"/>
          <span>Custom PC Builds</span>
        </section>

      </article>
    )
  }
};
